<?php

namespace eDiasoft\EpicmerceShop;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;

class EpicmerceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     */

    protected $namespace = 'eDiasoft\EpicmerceShop';

    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->registerSchedule();
        $this->registerCommands();
        $this->registerRoutes();
        $this->registerMiddlewares($router);
        $this->registerMigrations();

        $this->mergeConfigFrom(base_path('vendor/ediasoft/epicmerce-shop/src/config/filesystems.php'), 'filesystems.disks');
    }

    public function register()
    {
        //
    }

    /**
     * Register the service provider.
     */
    protected function registerRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Controller')
            ->group(base_path('vendor/ediasoft/epicmerce-shop/src/routes/shop.php'));

        Route::middleware(['web', 'auth.shop'])
            ->namespace($this->namespace . '\Controller')
            ->group(base_path('vendor/ediasoft/epicmerce-shop/src/routes/shop_auth.php'));

        Route::middleware('web')
            ->prefix('ajax')
            ->namespace($this->namespace . '\Controller\Ajax')
            ->group(base_path('vendor/ediasoft/epicmerce-shop/src/routes/ajax.php'));
    }

    protected function registerCommands()
    {
        $this->commands([
            Commands\SyncProduct::class
        ]);
    }

    protected function registerMiddlewares($router)
    {
        $router->pushMiddlewareToGroup('web', 'eDiasoft\EpicmerceShop\Middleware\InitApp');
        $router->pushMiddlewareToGroup('web', 'eDiasoft\EpicmerceShop\Middleware\SetCustomerLanguage');
        $router->pushMiddlewareToGroup('web', 'eDiasoft\EpicmerceShop\Middleware\ShoppingCart');

        $router->aliasMiddleware('auth.shop', 'eDiasoft\EpicmerceShop\Middleware\Auth');
        $router->aliasMiddleware('checkoutable', 'eDiasoft\EpicmerceShop\Middleware\Checkoutable');
    }

    protected function registerMigrations()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }

    protected function registerSchedule()
    {
        $this->app->booted(function () {
            $schedule = app(Schedule::class);
            $schedule->command('sync:products')->hourly();
        });
    }
}
