<?php
use Carbon\Carbon;
use eDiasoft\EpicmerceShop\Classes\Category;
use eDiasoft\EpicmerceShop\Model\Product;
use GuzzleHttp\Client;

if (! function_exists('guzzle')) 
{
    function guzzle($options = [])
    { 
        return new Client($options);
    }
}

if (! function_exists('categories')) 
{
    function categories()
    {
        if(!cache()->has('categories') || !env('CACHE'))
        {
            $categories = collect();
            $response = json_decode(epicmerce()->get('categories')->getBody());

            foreach(collect($response)->sortBy('rank') as $category)
            {
                $categories->push(new Category($category));
            }

            cache(['categories' => $categories], 720);
        }

        return cache('categories');
    }
}

if (! function_exists('countries')) 
{
    function countries()
    {
        if(!cache()->has('countries') || !env('CACHE'))
        {
            $response = json_decode(epicmerce()->get('countries')->getBody());

            cache(['countries' => $response], 720);
        }

        return cache('countries');
    }
}

if (! function_exists('all_countries')) 
{
    function all_countries()
    {
        if(!cache()->has('all_countries') || !env('CACHE'))
        {
            $response = json_decode(epicmerce()->get('countries?all=true')->getBody());

            cache(['all_countries' => $response], 720);
        }

        return cache('all_countries');
    }
}

if (! function_exists('currencies')) 
{
    function currencies()
    {
        if(!cache()->has('currencies') || !env('CACHE'))
        {
            $response = json_decode(epicmerce()->get('currencies')->getBody());

            if($response->status)
            {
                cache(['currencies' => collect($response->currencies)], 30);
            }
        }

        return cache('currencies');
    }
}

if (! function_exists('languages')) 
{
    function languages()
    {
        if(!cache()->has('languages') || !env('CACHE'))
        {
            $response = json_decode(epicmerce()->get('languages')->getBody());

            cache(['languages' => collect($response)], 720);
        }

        return cache('languages');
    }
}

if (! function_exists('carriers')) 
{
    function carriers()
    {
        if(!cache()->has('carriers') || !env('CACHE'))
        {
            $response = json_decode(epicmerce()->get('carriers')->getBody());

            if($response->status)
            {
                cache(['carriers' => $response->carriers], 720);
            }
        }

        return cache('carriers');
    }
}

if (! function_exists('ticket_departments')) 
{
    function ticket_departments()
    {
        $response = json_decode(epicmerce()->get('ticket/departments')->getBody());

        if(!cache()->has('ticket_departments') || !env('CACHE'))
        {
            if($response->status)
            {
                cache(['ticket_departments' => $response->departments], 720);
            }
        }

        return cache('ticket_departments');
    }
}

if (! function_exists('get_available_carrier_rules')) 
{
    function get_available_carrier_rules($address)
    {
        $carrier_rules = collect();

        $delivery_times = cart()->products->pluck('product.delivery_time')->unique()->values()->map(function($time){
            return array('delivery' => $time);
        });

        if($delivery_times->where('delivery', 0)->first() && $delivery_times->where('delivery', '>', 0)->first())
        {
            $direct_rules = collect();
            $external_rules = collect();

            foreach(collect(carriers())->where('active', 1) as $carrier)
            {
                foreach($carrier->rules as $rule)
                {
                    $direct_delivery_products = cart()->products->where('product.delivery_time', 0);
                    $external_delivery_products = cart()->products->where('product.delivery_time', '>', 0);

                    $direct_rule = new CarrierRule($carrier, $rule, $direct_delivery_products);
                    $external_rule = new CarrierRule($carrier, $rule, $external_delivery_products);

                    if($direct_rule->isUsable($address))
                    {
                        $direct_rules->push($direct_rule);
                    }

                    if($external_rule->isUsable($address))
                    {
                        $external_rules->push($external_rule);
                    }
                }
            }

            foreach($direct_rules as $direct_rule)
            {
                foreach($external_rules as $external_rule)
                {
                    if($direct_rule->code != $external_rule->code)
                    {
                        $carrier_rules->push([$direct_rule, $external_rule]);
                    }

                    if($direct_rule->code == $external_rule->code)
                    {
                        $carrier_rules->push($direct_rule);
                    }
                }
            }

            return additional_carrier_rules($address, $carrier_rules);
        }

        foreach(collect(carriers())->where('active', 1) as $carrier)
        {
            foreach($carrier->rules as $rule)
            {
                $rule = new CarrierRule($carrier, $rule);

                if($rule->isUsable($address))
                {
                    $carrier_rules->push($rule);
                }
            }
        }

        return additional_carrier_rules($address, $carrier_rules);
    }
}

if (! function_exists('payments')) 
{
    function payments()
    {
        if(!cache()->has('payments') || !env('CACHE'))
        {
            $response = json_decode(epicmerce()->get('payments')->getBody());

            if($response->status)
            {
                cache(['payments' => $response->payments], 720);
            }
        }

        return cache('payments');
    }
}

if (! function_exists('carbon')) 
{
    function carbon($params = null)
    {
        return new Carbon($params);
    }
}

if (! function_exists('price')) 
{
    function price($price, $symbol = "€")
    {
        return $symbol . " " . number_format($price, 2, ',', '.');
    }
}

if (! function_exists('user')) 
{
    function user()
    {
        if(session()->has('user'))
        {
            return session('user');
        }
        
        return false;
    }
}

if (! function_exists('cart')) 
{
    function cart()
    {
        return session('cart');
    }
}

if (! function_exists('epicmerce')) 
{
    function epicmerce()
    {
        return $client = new Client([
            'base_uri' => env('API_URI'),
            'headers'   => [
                "ShopToken"             => env('SHOP_TOKEN'),
                "Content-Type"          => "application/json",
                "Accept"                => "application/json",
                "CustomerToken"         => session('customer_session')
            ]
        ]);
    }
}

if (! function_exists('new_customer_session')) 
{
    function new_customer_session()
    {
        session()->setId(str_random(40));
        
        $customer_header = [
            "CustomerToken" => session()->getId()
        ];

        $body = collect([
            "customer_id"   => user()->id ?? null
        ]);

        $customer_response = epicmerce()->post('customer/init', ['headers' => $customer_header, 'body' => $body->toJson()]);

        if(json_decode($customer_response->getBody())->status)
        {
            session(['customer_session' => $customer_header["CustomerToken"]]);
        }

        return true;
    }
}

if (! function_exists('shop_info')) 
{
    function shop_info()
    {
        if(!cache()->has('shop') || !env('CACHE'))
        {
            $response = json_decode(epicmerce()->get('shop')->getBody());

            if($response->status)
            {
                cache(['shop' => $response->shop], 720);
            }
        }

        return cache('shop');
    }
}

if (! function_exists('infos')) 
{
    function infos($infos, $language_id = 1)
    {
        $info = collect($infos)->where('language_id', $language_id)->first();

        if(!$info)
        {
            $info = collect($infos)->first();
        }

        return $info;
    }
}

if (! function_exists('activity_log')) 
{
    function activity_log($level, $message)
    {
        $response = json_decode(epicmerce()->post('activity-log',[
            'body' => collect([
                'level'         => $level,
                'message'       => $message,
                'url'           => request()->url(),
                'ip'            => request()->ip()
            ])
        ])->getBody());

        return $response;
    }
}

if (! function_exists('super_user')) 
{
    function super_user()
    {
        if(session()->has('super_user'))
        {
            return true;
        }

        return false;
    }
}

if (! function_exists('googleMap')) 
{
    function googleMap($address)
    {
        return json_decode(guzzle()->get("https://maps.google.com/maps/api/geocode/json?address=" . $address . "&key=" . env('MAP_KEY'))->getBody());
    }
}

if (! function_exists('phone')) 
{
    function phone($phone)
    {
        $phone = str_replace(['-', ' '], '', strval($phone));

        $i = 0;

        foreach(str_split($phone) as $nr)
        {
            if($nr != 0)
            {
                break;  
            }

            $i++;
        }

        return substr($phone, $i);
    }
}

if (! function_exists('newest_products')) 
{
    function newest_products($limit)
    {
        $products = Product::latest()->limit($limit)->get();

        return $products;
    }
}

if (! function_exists('breadcrumb_lists')) 
{
    function breadcrumb_lists($lists)
    {
        $position = 1;
        
        $breadcrumb_lists = collect(array([
            "@type"     => "ListItem",
            "position"  => $position,
            "name"      => "Busukawa",
            "item"      => route('home')
        ]));

        foreach($lists as $list)
        {
            $breadcrumb_lists->push([
                "@type"     => "ListItem",
                "position"  => $position++,
                "name"      => $list["name"],
                "item"      => $list["url"]
            ]);
        }
        
        return $breadcrumb_lists;
    }
}



