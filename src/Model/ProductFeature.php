<?php
namespace eDiasoft\EpicmerceShop\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ProductFeature extends Model
{
    protected $fillable = [
    	'product_id',
    	'language_id'
    ];
}