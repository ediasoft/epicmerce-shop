<?php
namespace eDiasoft\EpicmerceShop\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use eDiasoft\EpicmerceShop\Classes\Price;
use eDiasoft\EpicmerceShop\Classes\CatalogPriceRule;

class Product extends Model
{
    use SoftDeletes;

    public $incrementing = false;
    
    protected $fillable = [
        'id',
        'images',
        'brand',
        'tax_rule_id',
        'product_combinations',
        'infos',
        'categories',
        'tags',
        'manufacture_sku',
        'ean',
        'upc',
        'isbn',
        'asin',
        'sku',
        'width',
        'height',
        'depth',
        'weight',
        'additional_fee_per_item',
        'total_stocks',
        'direct_stocks',
        'drop_stocks',
        'retail_price',
        'list_price',
        'featured',
        'active_when_out_of_stock',
        'deleted_at',
        'updated_at'
    ];

    protected static function boot()
    {
        parent::boot();

        if(!shop_info()->active_when_out_of_stock)
        {
            static::addGlobalScope('in_stock', function (Builder $builder) {
                $builder->where('total_stocks', '>', 0);
            });
        }
    }

    public function product_feature()
    {
        return $this->hasOne(ProductFeature::class);
    }
    
    public function categories()
    {
        return collect(json_decode($this->categories))->map(function($category){
            $category->data = categories()->where('id', $category->id)->first();
            $category->data->getLevel();
            
            return $category;
        });
    }

    public function category()
    {
        $category_id = collect($this->categories)->first()['id'];

        return categories()->where('id', $category_id)->first();
    }

    public function getPrice()
    {
        $this->listPrice = $this->listPrice ?? new Price($this->list_price, json_decode($this->tax_rule) ?? null, cart()->getBillingAddress()->country->id ?? null);
        $this->retailPrice = $this->retailPrice ?? new Price($this->retail_price, json_decode($this->tax_rule) ?? null, cart()->getBillingAddress()->country->id ?? null);
        $this->discount = 0;

        if($this->retailPrice->ex > 0 && $this->listPrice->ex > 0)
        {
            $this->discount = number_format(abs((($this->retailPrice->ex / $this->listPrice->ex)-1)*100), 2);
        }

        return $this;
    }

    public function images()
    {
        return collect(json_decode($this->images));
    }
}