<?php

namespace eDiasoft\EpicmerceShop\Middleware;

use Closure;

class InitApp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session()->has('customer_session') && session()->has('shop'))
        {
            return $next($request);
        }

        if(!session()->has('customer_session'))
        {
            new_customer_session();
        }

        if(!session()->has('shop'))
        {
            shop_info();
        }

        return $next($request);
    }
}
