<?php

namespace eDiasoft\EpicmerceShop\Middleware;

use Closure;
use App;

class SetCustomerLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('language'))
        {
            session(['language' => app()->getLocale()]);
        }

        if($request->has('language'))
        {
            session(['language' => $request->language]);
        }

        App::setLocale(session('language'));

        return $next($request);
    }
}
