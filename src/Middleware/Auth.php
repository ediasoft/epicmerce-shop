<?php

namespace eDiasoft\EpicmerceShop\Middleware;

use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(user())
        {
            return $next($request);
        }

        return redirect()->route('login');
    }
}
