<?php

namespace eDiasoft\EpicmerceShop\Middleware;

use Closure;
use eDiasoft\EpicmerceShop\Classes\Cart;

class ShoppingCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('cart'))
        {
            session(['cart' => new Cart]);
        }
        
        return $next($request);
    }
}
