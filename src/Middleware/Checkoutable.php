<?php

namespace eDiasoft\EpicmerceShop\Middleware;

use Closure;

class Checkoutable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(cart()->products->count() > 0)
        {
            return $next($request);
        }

        return redirect()->route('cart');
    }
}
