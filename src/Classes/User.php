<?php

namespace eDiasoft\EpicmerceShop\Classes;

class User
{
    public $id;
    public $customerable_id;
    public $customerable_type;
    public $customer_group_id;
    public $gender;
    public $first_name;
    public $last_name;
    public $dob;
    public $email;
    public $phone;
    public $phonenumber;
    public $company;
    public $vat_number;
    public $optin = true;
    public $active;
    public $deleted_at;
    public $created_at;
    public $updated_at;
    public $address;
    public $addresses;
    public $customer_group;

    public $last_updated;
    public $model;
    
    public function __construct($data)
    {
        $this->set($data);
    }

    public function set($data)
    {
        foreach($data as $key => $content)
        {
            $this->$key = $content;
        }

        $last_updated = carbon()->now();
    }

    public function register()
    {
        $data = collect([
            'gender'        => $this->gender,
            'first_name'    => $this->first_name,
            'last_name'     => $this->last_name,
            'dob'           => $this->dob,
            'phonenumber'   => $this->phonenumber,
            'email'         => $this->email,
            'password'      => $this->password,
            'company'       => $this->company,
            'vat_number'    => $this->vat_number,
            'street'        => $this->street,
            'housenumber'   => $this->housenumber,
            'city'          => $this->city,
            'zipcode'       => $this->zipcode,
            'country_id'    => $this->country_id,
            'optin'         => $this->optin,
        ])->toJson();

        $body = json_decode(epicmerce()->post('customer/register', ['body' => $data])->getBody());

        if($body->status)
        {
            $this->set($body->user);
        }

        return $this;
    }

    public function refreshAddressBook()
    {
        try {
            $body = json_decode(epicmerce()->get('customer/addresses')->getBody());

            if($body->status)
            {
                $this->addresses = $body->addresses;
            }
        } catch (Exception $e) {

        }
        
        return $this;
    }

    public function refresh()
    {
        try {
            $body = json_decode(epicmerce()->get('customer/me')->getBody());

            if($body->status)
            {
                $this->set($body->customer);
            }
        } catch (Exception $e) {

        }
        
        return $this;
    }

    public function acceptedTerms()
    {
        try {
            $body = json_decode(epicmerce()->get('customer/accept-terms')->getBody());

            $this->terms_accepted_at = carbon();
            
        } catch (Exception $e) {

        }
        
        return $this;
    }
}