<?php

namespace eDiasoft\EpicmerceShop\Classes;

class Price
{
    public function __construct($price, $tax_rule = null, $country_id = null)
    {
        $this->tax_rule = (object) $tax_rule;
        
        $this->country_id = $country_id;

        $this->ex = round($price, 2);
        $this->inc = round($price, 2);
        $this->taxes = collect();
        $this->totalTax = 0;

        $this->calculateTax();
        $this->calculateTotalTax();
    }

    public function calculateTax()
    {
        if(!empty((array) $this->tax_rule) && $this->tax_rule->active)
        {
            //Behavior == 0
            $this_tax_only = collect($this->tax_rule->lines)->where('country_id', $this->country_id)->where('behavior', 0)->first();

            if($this_tax_only)
            {
                $this_tax_only = (object) $this_tax_only;
                
                $this->addTax($this_tax_only);

                return $this;
            }
            
            //Behavior rest of it
            foreach(collect($this->tax_rule->lines)->where('country_id', $this->country_id) as $line)
            {
                $line = (object) $line;

                if($line->behavior == 1)
                {
                    //Under construction
                }

                if($line->behavior == 2)
                {
                    $this->addTax($line);
                }
            }
        }

        return $this;
    }

    private function addTax($line)
    {
        $line->tax = (object) $line->tax;
        $line->qty_tax = 0;
        $line->total_tax = 0;

        if($line->tax->rate)
        {
            $this->addTaxRate($line);
        }

        if($line->tax->percentage)
        {
            $this->addTaxPercentage($line);
        }

        $this->taxes->push($line);

        return $this;
    }

    private function calculateTotalTax()
    {
        $this->inc = round($this->inc, 3);
        $this->ex = round($this->ex, 3);

        $this->totalTax = round($this->inc - $this->ex, 2);
    }

    private function addTaxPercentage($line)
    {
        $line->tax = (object) $line->tax;
        $line->qty_tax += $this->inc * (floatval($line->tax->percentage) / 100);

        $this->inc = $this->inc * (1 + (floatval($line->tax->percentage) / 100));
        
        return $this;
    }

    private function addTaxRate($line)
    {
        $line->tax = (object) $line->tax;
        $line->qty_tax += floatval($line->tax->rate);

        $this->inc = $this->inc + floatval($line->tax->rate);

        return $this;
    }
}