<?php

namespace eDiasoft\EpicmerceShop\Classes;

class Category
{
    public function __construct($category)
    {
        $this->id = $category->id;
        $this->parent_id = $category->parent_id;
        $this->shop_id = $category->shop_id;
        $this->rank = $category->rank;
        $this->infos = collect($category->infos);
        $this->subcategories = $category->subcategories;
        $this->level = null;
    }
    
    public function info()
    {
        $info = $this->infos->where('language.iso', session('language'))->first();

        if(!$info)
        {
            $info = $this->infos->first();
        }

        return $info;
    }

    public function getLevel()
    {
        if(!$this->level)
        {
            $this->setLevel($this->parent_id);
        }

        return $this->level;
    }

    public function setLevel($parent_id, $level = 1)
    {
        if($parent_id)
        {
            $category = categories()->where('id', $parent_id)->first();

            $this->setLevel($category->parent_id, $level++);
        }

        $this->level = $level;

        return $this;
    }
}