<?php
namespace eDiasoft\EpicmerceShop\Classes;

class Address
{
    public $street;
    public $housenumber;
    public $zipcode;
    public $city;
    public $state;
    public $country_id;
    public $latitude;
    public $longitude;

    public function __construct($data)
    {
        foreach($data as $key => $info)
        {
            $this->$key = $info;
        }
    }

    public function getCoordinates()
    {
        if(!$this->latitude || !$this->longitude)
        {
            $this->updateCoordinates();
        }

        return $this;
    }

    private function updateCoordinates()
    {
        $address = implode("+", [
            $this->street,
            $this->housenumber,
            $this->zipcode,
            $this->city,
            $this->state
        ]);

        $result = collect(googleMap($this->street . "," . $this->zipcode . "," . $this->city . "," . "$this->state")->results)->first();

        $this->latitude = $result->geometry->location->lat ?? null;
        $this->longitude = $result->geometry->location->lng ?? null;
        
        return $this;
    }
}