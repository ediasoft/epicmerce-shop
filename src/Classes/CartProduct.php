<?php

namespace eDiasoft\EpicmerceShop\Classes;

class CartProduct
{
    public function __construct($product, $qty)
    {
        $this->product = $product;
        $this->qty = $qty;
        $this->total_price_ex_tax = 0;
        $this->total_price = 0;
        
        $this->calculateTotalPrice();
    }

    public function calculateTotalPrice($force = false)
    {
        $this->product->getPrice($force);

        $this->total_price = $this->product->retailPrice->inc * $this->qty;
        $this->total_price_ex_tax = $this->product->retailPrice->ex * $this->qty;
        
        return $this;
    }

    public function updateQty($qty, $static = false)
    {
        $this->qty += $qty;

        if($static)
        {
            $this->qty = $qty;
        }

        $this->calculateTotalPrice();
        
        return $this;
    }
}