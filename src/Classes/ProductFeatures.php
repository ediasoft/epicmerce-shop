<?php

namespace eDiasoft\EpicmerceShop\Classes;

class ProductFeatures
{
	public function __construct($products)
	{
		$this->products = $products;

		$this->features = collect();
		$this->brands = collect();
	}

	public function getFilters()
	{
		foreach($this->products as $product)
		{
			$this->collectFeatures($product);
			$this->collectBrands($product);
		}

		unset($this->products);

		return $this;
	}

	private function collectFeatures($product)
	{
		$features = collect($product->product_feature)->forget(['id', 'product_id', 'language_id', 'created_at', 'updated_at'])->keys();

		foreach($features as $feature)
		{
			$feature_value = $product->product_feature[$feature];

			if($feature_value)
			{
				$feature = $this->getFeature(strtolower($feature));
				$feature_option = $this->getFeatureOption($feature['options'], $feature_value);

				$feature_option->count++;
			}
		}
		
		return $this;
	}

	private function collectBrands($product)
	{
		if($product->brand)
		{
			$brand = $this->getBrand($product->brand);
			$brand->count++;
		}

		return $this;
	}

	private function getBrand($brand_name)
	{
		$brand = $this->brands->where('name', $brand_name)->first();

		if($brand)
		{
			return $brand;
		}

		$this->brands->push((object)[
			'name'	=> $brand_name,
			'count'	=> 0
		]);

		return $this->getBrand($brand_name);
	}

	public function getFeature($feature_name)
	{
		$feature = $this->features->where('name', $feature_name)->first();
		
		if($feature)
		{
			return $feature;
		}

		$this->features->push([
		 	'name'		=> $feature_name,
		 	'options'	=> collect()
		]);

		return $this->getFeature($feature_name);
	}

	public function getFeatureOption($options, $feature_value)
	{
		$feature_option = $options->where('name', $feature_value)->first();
		
		if($feature_option)
		{
			return $feature_option;
		}

		$options->push((object)[
		 	'name'		=> $feature_value,
		 	'count'		=> 0
		]);

		return $this->getFeatureOption($options, $feature_value);
	}
}