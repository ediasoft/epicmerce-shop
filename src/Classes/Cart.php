<?php

namespace eDiasoft\EpicmerceShop\Classes;

class Cart
{
    public function __construct()
    {
        $this->products = collect();
        $this->currency_id = 2;
        $this->total_qty = 0;
        $this->shipping = 0;
        $this->subtotal = 0;
        $this->total = 0;
        $this->totalTax = 0;

        $this->cart_rule = null;

        $this->addresses = collect();
        $this->shipping_rules = collect();
        $this->payment = collect();
        $this->taxes = collect();

        $this->customer_comment = null;
    }

    public function isEmpty()
    {
        if($this->products->count() > 0)
        {
            return false;
        }

        return true;
    }

    public function setAddress()
    {
        $default_delivery_address = collect(user()->addresses)->where('default', 1)->first();

        cart()->addresses = collect([
            "billing" => new Address([
                "first_name"    => user()->first_name,
                "last_name"     => user()->last_name,
                "company"       => user()->company,
                "street"        => user()->address->street,
                "housenumber"   => user()->address->housenumber,
                "city"          => user()->address->city,
                "zipcode"       => user()->address->zipcode,
                "country_id"    => user()->country_id,
                "country"       => user()->country ?? null
            ]),
            "delivery" => new Address([
                "first_name"            => $default_delivery_address->first_name ?? user()->first_name,
                "last_name"             => $default_delivery_address->last_name ?? user()->last_name,
                "company"               => $default_delivery_address->company ?? user()->company,
                "street"                => $default_delivery_address->address->street ?? user()->address->street,
                "housenumber"           => $default_delivery_address->address->housenumber ?? user()->address->housenumber,
                "housenumber_addition"  => $default_delivery_address->address->housenumber_addition ?? user()->address->housenumber_addition ?? null,
                "city"                  => $default_delivery_address->address->city ?? user()->address->city,
                "zipcode"               => $default_delivery_address->address->zipcode ?? user()->address->zipcode,
                "country_id"            => $default_delivery_address->address->country_id ?? user()->address->country_id,
                "country"               => $default_delivery_address->address->country ?? user()->address->country,
            ])
        ]);

        return $this;
    }

    public function getBillingAddress()
    {
        if(isset($this->addresses['billing']))
        {
            return $this->addresses['billing'];
        }

        if(user())
        {
            $this->setAddress();

            return $this->getBillingAddress();
        }
        
        return null;
    }
    
    public function refresh()
    {
        $this->products->map(function($product){
            $product->calculateTotalPrice(true);
        });
    }

    public function update($product, $qty, $static)
    {
        $cart_product = $this->products->where('product.id', $product->id)->first();

        if($cart_product)
        {
            $cart_product->updateQty($qty, $static);

            $this->removeNullQtyProducts();
            $this->getTaxes();
            $this->calcCartTotalPrice();

            return $this;
        }

        $this->products->push(new CartProduct($product, $qty));

        $this->getTaxes();
        $this->calcCartTotalPrice();

        return $this;
    }

    public function removeNullQtyProducts()
    {
        $this->products = $this->products->where('qty', '>', 0);
    }

    public function calcCartTotalPrice()
    {
        $this->calcCartTotalQty();

        $this->subTotalPrice();

        $this->calcTax();

        $this->totalPrice();

        return $this;
    }

    public function calcCartTotalQty()
    {
        $total_qty = 0;

        foreach($this->products as $product)
        {
            $total_qty += $product->qty;
        }

        $this->total_qty = $total_qty;

        return $this;
    }

    public function subTotalPrice()
    {
        $total_price = 0;

        foreach($this->products as $cart_product)
        {
            $total_price += $cart_product->total_price_ex_tax;
        }

        $this->subtotal = $total_price;

        return $this;
    }

    public function calcTax()
    {
        $this->totalTax = 0;

        foreach($this->taxes as $tax)
        {
            $this->totalTax += $tax->total_tax;
        }

        return $this;
    }

    public function getTaxes()
    {
        $this->taxes = collect();

        foreach($this->products as $cart_product)
        {
            foreach($cart_product->product->retailPrice->taxes as $product_tax)
            {
                $product_tax->total_tax = $product_tax->qty_tax * $cart_product->qty;

                $tax = $this->taxes->where('tax_id', $product_tax->tax_id)->first();

                if(!$tax)
                {
                    $this->taxes->push($product_tax);

                    continue;
                }

                $tax->total_tax += $product_tax->total_tax;
            }
        }

        return $this;
    }

    public function totalPrice()
    {
        $this->calculateCoupon();

        $this->total = $this->subtotal + $this->totalTax +$this->shipping;

        return $this;
    }

    public function toJson()
    {
        return collect($this)->toJson();
    }

    public function applyCoupon($cart_rule)
    {
        $this->cart_rule = $cart_rule;
        
        $this->calcCartTotalPrice();

        return $this;
    }

    private function calculateCoupon()
    {
        if(!$this->cart_rule)
        {
            return $this;
        }

        if($this->cart_rule->free_shipping)
        {
            $this->shipping = 0;
        }
        
        $tax_percentage = $this->vat_tax / $this->subtotal;

        if($this->cart_rule->is_inc_tax)
        {
            if($this->cart_rule->is_amount)
            {
                $this->vat_tax = $this->vat_tax - ($this->cart_rule->amount * $tax_percentage);
                $this->subtotal = $this->subtotal - ($this->cart_rule->amount - ($this->cart_rule->amount * $tax_percentage));
            }

            if(!$this->cart_rule->is_amount)
            {
                $this->vat_tax = $this->vat_tax * ((100 - $this->cart_rule->amount) / 100);
                $this->subtotal = $this->subtotal * ((100 - $this->cart_rule->amount) / 100);
            }
        }

        if(!$this->cart_rule->is_inc_tax)
        {
            if($this->cart_rule->is_amount)
            {
                $this->subtotal = $this->subtotal - $this->cart_rule->amount;
            }

            if(!$this->cart_rule->is_amount)
            {
                $this->subtotal = $this->subtotal * ((100 - $this->cart_rule->amount) / 100);
            }

            $this->vat_tax = $this->subtotal * $tax_percentage;
        }

    }

    public function sync()
    {
        try {
            cms_rest()->post('cart/sync', ['body' => $this->toJson()]);
        } catch (Exception $e) {

        }
        

        return $this;
    }

    public function isCheckedOut()
    {
        try {
            $response = json_decode(cms_rest()->get('customer/session')->getBody());
        } catch (Exception $e) {

        }

        if($response->status && $response->session->order && in_array($response->session->order->order_status_id, [8, 16, 2, 10, 12, 1, 9, 11, 13]))
        {
            return true;
        }

        return false;
    }
}