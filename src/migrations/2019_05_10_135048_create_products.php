<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->integer('id');
            $table->json('images')->nullable();
            $table->string('brand')->nullable();
            $table->integer('tax_rule_id')->default(0);
            $table->json('product_combinations');
            $table->json('infos')->nullable();
            $table->json('categories')->nullable();
            $table->json('tags')->nullable();
            $table->string('manufacture_sku')->nullable();
            $table->string('ean')->nullable();
            $table->string('upc')->nullable();
            $table->string('isbn')->nullable();
            $table->string('asin')->nullable();
            $table->string('sku');
            $table->decimal('width', 10, 2)->default(0)->nullable();
            $table->decimal('height', 10, 2)->default(0)->nullable();
            $table->decimal('depth', 10, 2)->default(0)->nullable();
            $table->decimal('weight', 10, 2)->default(0)->nullable();
            $table->decimal('additional_fee_per_item', 10, 2)->default(0)->nullable();
            $table->integer('total_stocks')->default(0);
            $table->integer('direct_stocks')->default(0);
            $table->integer('drop_stocks')->default(0);
            $table->decimal('retail_price', 10, 2)->default(0);
            $table->decimal('list_price', 10, 2)->nullable()->default(0);
            $table->boolean('featured')->default(false);
            $table->integer('min_delivery_days')->nullable();
            $table->integer('max_delivery_days')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
