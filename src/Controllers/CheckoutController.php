<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;
use eDiasoft\EpicmerceShop\Classes\User;
use eDiasoft\EpicmerceShop\Classes\Cart;
use eDiasoft\EpicmerceShop\Controller\Auth\LoginController;

class CheckoutController extends Controller
{
    public function checkout()
    {
        return view('checkout');
    }

    public function process()
    {
        $this->setUser();
        $this->setCartAddress();
        $this->setShipping();
        $this->setPayment();

        return redirect()->route('pay');
    }

    public function setUser()
    {
        $user = new User([
            "gender" => null,
            "password"      => bcrypt(str_random(8)),
        ]);

        if(user())
        {
            $user = user();
        }

        $user->set([
            "first_name"    => $this->request->billing['first_name'],
            "last_name"     => $this->request->billing['last_name'],
            "email"         => $this->request->email,
            "phonenumber"   => $this->request->billing['phone'],
            "company"       => $this->request->billing['company'],
            "street"        => $this->request->billing['street'],
            "housenumber"   => $this->request->billing['housenumber'],
            "city"          => $this->request->billing['city'],
            "zipcode"       => $this->request->billing['zipcode'],
            "country_id"    => $this->request->billing['country_id']
        ]);

        if(!user())
        {
            $user->register();
            session(['user' => $user]);
        }

        return $user;
    }

    public function setCartAddress()
    {
        cart()->addresses = collect([
            "billing" => [
                "first_name"    => $this->request->billing['first_name'],
                "last_name"     => $this->request->billing['last_name'],
                "phonenumber"   => $this->request->billing['phone'],
                "company"       => $this->request->billing['company'],
                "street"        => $this->request->billing['street'],
                "housenumber"   => $this->request->billing['housenumber'],
                "city"          => $this->request->billing['city'],
                "zipcode"       => $this->request->billing['zipcode'],
                "country_id"    => $this->request->billing['country_id']
            ],
            "delivery" => [
                "first_name"    => ($this->request->other_delivery_address)? $this->request->deliver['first_name'] : $this->request->billing['first_name'],
                "last_name"     => ($this->request->other_delivery_address)? $this->request->deliver['last_name'] : $this->request->billing['last_name'],
                "phonenumber"   => ($this->request->other_delivery_address)? $this->request->deliver['phone'] : $this->request->billing['phone'],
                "company"       => ($this->request->other_delivery_address)? $this->request->deliver['company'] : $this->request->billing['company'],
                "street"        => ($this->request->other_delivery_address)? $this->request->deliver['street'] : $this->request->billing['street'],
                "housenumber"   => ($this->request->other_delivery_address)? $this->request->deliver['housenumber'] : $this->request->billing['housenumber'],
                "city"          => ($this->request->other_delivery_address)? $this->request->deliver['city'] : $this->request->billing['city'],
                "zipcode"       => ($this->request->other_delivery_address)? $this->request->deliver['zipcode'] : $this->request->billing['zipcode'],
                "country_id"    => ($this->request->other_delivery_address)? $this->request->deliver['country_id'] : $this->request->billing['country_id']
            ]
        ]);

        return $this;
    }

    public function setShipping()
    {
        cart()->carrier_id = $this->request->carrier_id;

        return $this;
    }

    public function setPayment()
    {
        $payment['id'] = $this->request->payment_id;

        if($this->request->payment_id == 1)
        {
            $payment['bank'] = $this->request->ideal_bank;
        }

        cart()->payment = collect($payment);

        return $this;
    }

    public function pay()
    {
        $payment = json_decode(epicmerce()->post('order/new', ['body' => collect(['cart' => session('cart')])->toJson()])->getBody());

        if(!$payment->data->finish)
        {
            return redirect($payment->data->redirect_uri);
        }

        if($payment->data->finish)
        {
            //session(['testing' => $payment->data->order]);
            return redirect()->route('checkout-finish')->with('instructions', $payment->data->instructions)->with('order', $payment->data->order);
        }

        return redirect()->route('home');
    }

    public function finish()
    {
        if(session()->has('order'))
        {
            new_customer_session();

            session(['cart' => new Cart]);

            return view('checkout_finish');
        }
        
        return redirect()->route('my-orders');
    }
}