<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;

use Storage;

class FileController extends Controller
{
    public function getFile()
    {
    	$size = $this->request->size ?? 'md';

    	return redirect(Storage::url(session('shop')->id . '/' . $this->request->model . '/' . $this->request->id . '/' . $size . '/' . $this->request->filename));
    }
}

