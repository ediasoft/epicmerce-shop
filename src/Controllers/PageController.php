<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;
use eDiasoft\EpicmerceShop\Model\Product;

class PageController extends Controller
{
    public function faq()
    {
    	return view('faq');
    }

    public function tag()
    {
    	$products = Product::whereJsonContains('tags', $this->request->tag)->paginate(config('app.paginate_product_limit'));

    	return view('tag', compact('products'));
    }
}
