<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;
use eDiasoft\EpicmerceShop\Model\Product;

class SitemapController extends Controller
{
    public function sitemap()
    {
    	$products = Product::get();
    	$tags = collect();

    	$products->pluck('tags')->map(function($product_tags) use ($tags){
    		$tags->push(json_decode($product_tags));
    	});

    	$tags = array_unique($tags->flatten()->toArray());

    	return response()->view('sitemap', compact('products', 'tags'))->header('Content-Type', 'text/xml');
    }
}
