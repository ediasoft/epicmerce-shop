<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;
use eDiasoft\EpicmerceShop\Model\Product;
use eDiasoft\EpicmerceShop\Classes\Search;

class SearchController extends Controller
{
    public function search()
    {
        $products = Product::paginate(30);

        return view('search', compact('products'));
    }
}