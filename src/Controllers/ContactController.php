<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function contact()
    {
        return view('contact');
    }

    public function createTicket()
    {
        $data = collect([
            "language"      => session('language'),
            "name"          => $this->request->name,
            "email"         => $this->request->email,
            "department"    => $this->request->department,
            "reference"     => $this->request->reference,
            "body"          => $this->request->body,
            "ip"            => $this->request->ip()
        ]);

        $ticket = json_decode(app()->rest->post('ticket/new', ['body' => $data->toJson()])->getBody());

        if($ticket->status)
        {
            return redirect()->route('ticket', $ticket->ticket->reference);
        }

        return redirect()->route('contact');
    }
}
