<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function home()
    {
    	return view('home');
    }
}
