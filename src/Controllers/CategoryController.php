<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;
use eDiasoft\EpicmerceShop\Model\Product;

class CategoryController extends Controller
{
    public function category()
    {
    	$category = categories()->where('id', $this->request->id)->first();
    	
    	$products = Product::whereRaw('JSON_CONTAINS(categories, \'{"id": ' . $category->id . '}\')')->paginate(config('app.paginate_product_limit'));

    	return view('category', compact('category', 'products'));
    }
}