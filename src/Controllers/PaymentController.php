<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function callback()
    {
        $data = null;

        if($this->request->provider == 'paynl')
        {
            $data = $this->getPayNLData();
        }
        
        if($data)
        {
            $payment = json_decode(app()->rest->post('payment/callback', ['body' => $data->toJson()])->getBody());

            if($payment->order)
            {
                return redirect()->route('checkout-finish')->with('order', $payment->order);
            }
        }
    }

    public function getPayNLData()
    {
        return collect([
            'provider'          => $this->request->provider,
            'payment'           => $this->request->payment,
            'orderId'           => $this->request->orderId,
            'orderStatusId'     => $this->request->orderStatusId,
            'paymentSessionId'  => $this->request->paymentSessionId
        ]);
    }
}
