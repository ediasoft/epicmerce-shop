<?php

namespace eDiasoft\EpicmerceShop\Controller\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use eDiasoft\EpicmerceShop\Classes\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        if(user())
        {
            return redirect()->route('my-profile');
        }

        return view('login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $data = collect([
            'email' => $request->email,
            'password'  => $request->password
        ]);

        $login = json_decode(app()->rest->post('customer/login', ['body' => $data->toJson()])->getBody());

        if($login->status)
        {
            session(['user' => new User($login->user)]);

            return redirect()->back();
        }

        return redirect()->route('login');
    }
}
