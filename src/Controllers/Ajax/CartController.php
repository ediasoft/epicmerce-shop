<?php
namespace eDiasoft\EpicmerceShop\Controller\Ajax;

use App\Http\Controllers\Controller;
use eDiasoft\EpicmerceShop\Model\Product;

class CartController extends Controller
{
    public function update()
    {
    	$product = Product::where('id', intval($this->request->product_id))->first();

    	session('cart')->update($product, $this->request->qty, $this->request->static);

    	return $this->cart();
    }

    public function cart()
    {
    	return array(
    		'status'	=> true,
    		'cart'		=> session('cart')
    	);
    }
}