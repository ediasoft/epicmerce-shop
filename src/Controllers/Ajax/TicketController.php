<?php
namespace eDiasoft\EpicmerceShop\Controller\Ajax;

use App\Http\Controllers\Controller;

class TicketController extends Controller
{
	public function newMessage()
	{
		$data = collect([
            "body"			=> $this->request->message,
            "ip"			=> $this->request->ip()
        ]);

		$message = app()->rest->post('ticket/' . $this->request->reference . '/new', ['body' => $data->toJson()])->getBody();

		return $message;
	}
}