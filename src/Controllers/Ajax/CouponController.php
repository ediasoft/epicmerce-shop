<?php
namespace eDiasoft\EpicmerceShop\Controller\Ajax;

use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    public function redeem()
    {
        $response = json_decode(app()->rest->get('cart-rule/redeem?code=' . $this->request->code)->getBody());

        if($response->status)
        {
            cart()->applyCoupon($response->cart_rule);

            return array('status' => true, 'cart' => cart());
        }
        
        return array('status' => false);
    }

    public function remove()
    {
        cart()->cart_rule = null;
        cart()->calcCartTotalPrice();
        
        return array('status' => true, 'cart' => cart());
    }
}