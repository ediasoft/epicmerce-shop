<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;
use eDiasoft\EpicmerceShop\Model\Product;

class ProductController extends Controller
{
    public function product()
    {
        $product = Product::with(['product_feature' => function($query){
            $query->where('language_id', languages()->where('iso', app()->getLocale())->first()->id);
        }])->where('id', intval($this->request->id))->firstOrFail();
        $breadcrumb_lists = collect();

        foreach($product->categories()->sortBy('parent_id') as $category)
        {
            $breadcrumb_lists->push([
                'name'      => infos($category->data->infos)->name,
                'url'       => route('category', [$category->id, str_slug(infos($category->data->infos)->name)]),
                'level'     => $category->data->getLevel()
            ]);
        }

        $breadcrumb_lists->push([
            'name'      => infos(json_decode($product->infos))->name,
            'url'       => route('product', [$product->id, str_slug(infos(json_decode($product->infos))->name)])
        ]);

        return view('product', compact('product', 'breadcrumb_lists'));
    }
}