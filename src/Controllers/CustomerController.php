<?php
namespace eDiasoft\EpicmerceShop\Controller;

use eDiasoft\EpicmerceShop\Classes\User;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function profile()
    {
        return view('profile');
    }

    public function updateProfile()
    {
        $data = collect([
            "company"                   => $this->request->company,
            "first_name"                => $this->request->first_name,
            "last_name"                 => $this->request->last_name,
            "email"                     => $this->request->email,
            "password"                  => $this->request->password,
            "phonenumber_country"       => $this->request->phonenumber_country,
            "phonenumber"               => $this->request->phonenumber,
            "street"                    => $this->request->street,
            "housenumber"               => $this->request->housenumber,
            "city"                      => $this->request->city,
            "zipcode"                   => $this->request->zipcode,
            "state"                     => $this->request->state,
            "country_id"                => $this->request->country_id
        ]);

        $profile = json_decode(app()->rest->post('customer', ['body' => $data->toJson()])->getBody());

        if($profile->status)
        {
            user()->set($profile->user);
            
            return redirect()->route('my-profile');
        }

        return redirect()->route('my-profile')->with('error', $profile->message);
    }

    public function orders()
    {
        $result = json_decode(app()->rest->get('customer/orders')->getBody());
        
        if($result->status)
        {
            $order_data = $result->orders;
            return view('orders', compact('order_data'));
        }

        return redirect()->route('home');
    }

    public function viewOrder()
    {
        $order = json_decode(app()->rest->get('customer/order/' . $this->request->reference)->getBody());

        if($order->status)
        {
            $order = $order->order;
            return view('view_order', compact('order'));
        }

        return redirect()->route('my-orders');
    }

    public function addresses()
    {
        $result = json_decode(app()->rest->get('customer/addresses')->getBody());

        if($result->status)
        {
            $address_data = $result->addresses;
            return view('addresses', compact('address_data'));
        }

        return redirect()->route('home');
    }

    public function editAddress()
    {
        $result = json_decode(app()->rest->get('customer/address/' . $this->request->id)->getBody());
        
        if($result->status)
        {
            $address = $result->address;
            return view('edit_address', compact('address'));
        }

        return redirect()->route('home');
    }

    public function updateAddress()
    {
        $data = collect([
            "id"            => $this->request->id,
            "alias"         => $this->request->alias,
            "company"       => $this->request->company,
            "first_name"    => $this->request->first_name,
            "last_name"     => $this->request->last_name,
            "street"        => $this->request->street,
            "housenumber"   => $this->request->housenumber,
            "city"          => $this->request->city,
            "zipcode"       => $this->request->zipcode,
            "state"         => $this->request->state,
            "country_id"    => $this->request->country_id
        ]);

        $address = json_decode(app()->rest->post('customer/address/' . $this->request->id, ['body' => $data->toJson()])->getBody());

        if($address->status)
        {
            user()->addresses = collect(user()->addresses)->transform(function ($item, $key) use ($address) {
                if($item->id == $address->address->id)
                {
                    $item = $address->address;
                }

                return $item;
            });
        }
        
        return redirect()->route('my-addresses');
    }

    public function setDefaultAddress()
    {
        json_decode(app()->rest->get('customer/address/default/' . $this->request->id)->getBody());

        return redirect()->route('my-addresses');
    }

    public function removeDefaultAddress()
    {
        json_decode(app()->rest->get('customer/address/remove/' . $this->request->id)->getBody());

        return redirect()->route('my-addresses');
    }
}
