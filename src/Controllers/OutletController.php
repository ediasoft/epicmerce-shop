<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;

class OutletController extends Controller
{
    public function outlet()
    {
    	return view('outlet');
    }
}