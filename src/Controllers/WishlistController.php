<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;

class WishlistController extends Controller
{
    public function wishlist()
    {
        return view('wishlist');
    }
}
