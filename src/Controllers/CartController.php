<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;
use eDiasoft\EpicmerceShop\Model\Product;

class CartController extends Controller
{
    public function update()
    {
        $product = Product::where('id', intval($this->request->product_id))->with('product_feature')->first();

        session('cart')->update($product, $this->request->qty, $this->request->static);

        return redirect()->route('cart');
    }

    public function cart()
    {
        return view('cart');
    }
}