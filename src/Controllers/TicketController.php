<?php
namespace eDiasoft\EpicmerceShop\Controller;

use App\Http\Controllers\Controller;

class TicketController extends Controller
{
	public function viewTicket()
	{
		$ticket = json_decode(app()->rest->get('ticket/' . $this->request->reference)->getBody());

		if($ticket->status)
		{
			$ticket = $ticket->ticket;
			
			return view('ticket', compact('ticket'));
		}
		
		abort(404);
	}
}