<?php
Route::get('orders', ['uses' => 'OrderController@orders']);
Route::get('order/{id}', ['uses' => 'OrderController@order']);