<?php
Route::get('my-account/profile', ['uses' => 'CustomerController@profile'])->name('my-profile');
Route::post('my-account/profile', ['uses' => 'CustomerController@updateProfile']);

Route::get('my-account/orders', ['uses' => 'CustomerController@orders'])->name('my-orders');
Route::get('my-account/order/{reference}', ['uses' => 'CustomerController@viewOrder'])->name('view-order');
Route::get('my-account/order/{reference}/invoice', ['uses' => 'OrderController@invoice'])->name('invoice');

Route::get('my-account/addresses', ['uses' => 'CustomerController@addresses'])->name('my-addresses');
Route::get('my-account/address/edit/{id}', ['uses' => 'CustomerController@editAddress'])->name('edit-my-address');
Route::get('my-account/address/default/{id}', ['uses' => 'CustomerController@setDefaultAddress'])->name('set-default-my-address');
Route::get('my-account/address/remove/{id}', ['uses' => 'CustomerController@removeDefaultAddress'])->name('remove-my-address');

Route::post('my-account/address/edit/{id}', ['uses' => 'CustomerController@updateAddress']);

Route::get('my-account/returns', ['uses' => 'CustomerController@returns'])->name('my-returns');
Route::get('my-account/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');