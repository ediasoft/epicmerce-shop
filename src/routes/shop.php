<?php

Route::get('/', ['uses' => 'HomeController@home'])->name('home');

Route::get('product/{id}/{name}', ['uses' => 'ProductController@product'])->name('product');

Route::get('category/{id}/{name}', ['uses' => 'CategoryController@category'])->name('category');
Route::get('cart', ['uses' => 'CartController@cart'])->name('cart');

Route::post('cart/add/{product_id}', ['uses' => 'CartController@update'])->name('add-to-cart');

Route::get('wishlist', ['uses' => 'WishlistController@wishlist'])->name('wishlist');

Route::get('search', ['uses' => 'SearchController@search'])->name('search');

Route::get('outlet', ['uses' => 'OutletController@outlet'])->name('outlet');

Route::get('faq', ['uses' => 'PageController@faq'])->name('faq');
Route::get('privacy', ['uses' => 'PageController@privacy'])->name('privacy');
Route::get('terms-of-service', ['uses' => 'PageController@terms'])->name('terms');

Route::get('contact', ['uses' => 'ContactController@contact'])->name('contact');
Route::post('contact', ['uses' => 'ContactController@createTicket']);

Route::get('ticket/{reference}', ['uses' => 'TicketController@viewTicket'])->name('ticket');

Route::get('payment/{provider}/{payment}/callback', ['uses' => 'PaymentController@callback']);

Route::get('my-account/profile', ['uses' => 'CustomerController@profile'])->name('profile');

Route::get('page/{id}/{name}', ['uses' => 'PageController@page'])->name('pages');
Route::get('tag/{tag}', ['uses' => 'PageController@tag'])->name('tag');

Route::get('blogs', ['uses' => 'BlogController@blogs'])->name('blogs');
Route::get('blog/{id}/{name}', ['uses' => 'BlogController@blog'])->name('blog');

Route::get('file/{model}/{id}/{filename}', ['uses' => 'FileController@getFile'])->name('file');

Route::get('login', ['uses' => 'Auth\LoginController@showLoginForm'])->name('login');
Route::post('login', ['uses' => 'Auth\LoginController@login']);

Route::get('sitemap', ['uses' => 'SitemapController@sitemap'])->name('sitemap');

Route::middleware(['checkoutable'])->group(function () {
	Route::get('checkout', ['uses' => 'CheckoutController@checkout'])->name('checkout');
	Route::post('checkout', ['uses' => 'CheckoutController@process']);

	Route::get('checkout/pay', ['uses' => 'CheckoutController@pay'])->name('pay');
	Route::get('checkout/finish', ['uses' => 'CheckoutController@finish'])->name('checkout-finish');
});