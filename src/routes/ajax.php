<?php
Route::get('filters', 'ProductController@getFilters');

Route::get('orders', ['uses' => 'OrderController@orders']);
Route::get('order/{id}', ['uses' => 'OrderController@order']);

Route::post('carriers', 'ShippingController@carriers');

Route::get('cart', ['uses' => 'CartController@cart']);
Route::get('cart/sync', ['uses' => 'CartController@syncCart']);

Route::post('cart/update', ['uses' => 'CartController@update']);
Route::post('cart/calculate', ['uses' => 'CartController@calculate']);

Route::post('dutch-zipcode-finder', ['uses' => 'AddressController@dutchZipcode']);

Route::get('coupon/redeem', ['uses' => 'CouponController@redeem']);
Route::get('coupon/remove', ['uses' => 'CouponController@remove']);

Route::get('ticket/{reference}/messages', ['uses' => 'TicketController@getMessages']);

Route::post('ticket/{reference}/new', ['uses' => 'TicketController@newMessage']);