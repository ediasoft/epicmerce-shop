<?php

return [
    'gcs' => [
        'driver' => 'gcs',
        'project_id' => env('GOOGLE_CLOUD_PROJECT_ID', 'ediasoft-cloud'),
        'key_file' => env('GOOGLE_CLOUD_KEY_FILE', storage_path('storage-credentials.json')), // optional: /path/to/service-account.json
        'bucket' => env('GOOGLE_CLOUD_STORAGE_BUCKET', 'epicmerce'),
        'path_prefix' => env('GOOGLE_CLOUD_STORAGE_PATH_PREFIX', null), // optional: /default/path/to/apply/in/bucket
        'storage_api_uri' => env('GOOGLE_CLOUD_STORAGE_API_URI', null), // see: Public URLs below
    ],
	'gcs_sync' => [
        'driver' => 'gcs',
        'project_id' => env('GOOGLE_CLOUD_PROJECT_ID', 'ediasoft-cloud'),
        'key_file' => env('GOOGLE_CLOUD_KEY_FILE', storage_path('storage-credentials.json')), // optional: /path/to/service-account.json
        'bucket' => env('GOOGLE_CLOUD_STORAGE_PRIVATE_BUCKET', 'mulan'),
        'path_prefix' => env('GOOGLE_CLOUD_STORAGE_PATH_PREFIX', null), // optional: /default/path/to/apply/in/bucket
        'storage_api_uri' => env('GOOGLE_CLOUD_STORAGE_API_URI', null), // see: Public URLs below
    ]
];