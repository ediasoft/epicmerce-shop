<?php

namespace eDiasoft\EpicmerceShop\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

use eDiasoft\EpicmerceShop\Model\Product;
use eDiasoft\EpicmerceShop\Model\Sync;
use League\Csv\Reader;
use League\Csv\Statement;
use Storage;
use App\Classes\SyncProductFeature;

class SyncProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync products';

    protected $bar;
    protected $last_page;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sync = json_decode(epicmerce()->get('syncs/products')->getBody());

        if($sync->status)
        {
            $this->start = carbon();

            if(env('APP_ENV') == 'production')
            {
                $file = Storage::disk('gcs_sync')->get('syncs/products/' . $sync->file->filename);
            }

            if(env('APP_ENV') != 'production')
            {
                $file = Storage::disk('public')->get('syncs/products/' . $sync->file->filename);
            }

            $products = Reader::createFromString($file);
            $bar = $this->output->createProgressBar(count($products));
            $products->setHeaderOffset(0);

            foreach($products->getRecords() as $sync_product)
            {
                $sync_product = collect($sync_product)->map(function($value){
                    if($value == "")
                    {
                        return null;
                    }

                    return $value;
                })->toArray();

                $sync_product['deleted_at'] = null;
                $sync_product['updated_at'] = carbon();

                $product = Product::withTrashed()->withoutGlobalScopes()->updateOrCreate(
                    ['id' => intval($sync_product['id'])],
                    $sync_product
                );

                SyncProductFeature::sync($product, $sync_product['features']);

                $bar->advance();
            }

            $bar->finish();

            $this->removeNotUpdatedProducts();

            epicmerce()->delete('syncs/file/' . $sync->file->id);

            guzzle([
                'base_uri' => env('API_URI'),
                'headers'   => [
                    "ShopToken"             => env('SHOP_TOKEN'),
                    "Accept-Language"       => env('SHOP_LANG'),
                    "Content-Type"          => "application/json",
                    "Accept"                => "application/json"
                ]
            ])->post('activity-log', ['body' => collect([
                'level'                     => 1,
                'message'                   => 'Shop products (' . count($products) . ') synced. It took ' .  carbon()->diffInMinutes($this->start) . ' minutes.'
            ])->toJson()]);
        }

        return $this;
    }

    protected function removeNotUpdatedProducts()
    {
       Product::where('updated_at', '<', $this->start)->delete();
        
        return $this;
    }
}
